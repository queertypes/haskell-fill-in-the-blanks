{-

Welcome to the unit test module for: Functions Acquired!

We'll be using the Tasty test framework along with an HUnit extension
for Tasty that allows us to write unit-style tests.

Read below to see what tests are like in this style. You're also
welcome to extend the test suite below to ensure your implementation
is correct.

To run in summary mode:

$ cabal test unit

To run verbosely and colorfully, build the tests once and then:

$ ./dist/build/unit/unit

Have fun!

-}
module Main where

import Prelude hiding (
  map, filter, foldr, foldl, sum, product, and, or, any, all, (.),
  reverse, take, drop, zip
  )


import Test.Tasty
import Test.Tasty.HUnit

import FunctionsAcquired

----------------------------------------------------------------------
                      -- Test Infrastructure --
                         -- Bonuses Below --
----------------------------------------------------------------------

-- | Unit tests; good for exploring the domain
-- These get verbose quickly, though!
-- It's often better to tinker with the REPL until
-- the properties of the functions in question start to show through

-- For system properties ("integration" tests), spec-tests are usually
-- more suitable

test :: TestTree
test = testGroup "Unit tests" [
  testGroup "map"
     [ tC "map1" $ map (+1) [1..10] @?= [2..11]
     , tC "map2" $ map (+1) [] @?= []
     ]

  , testGroup "filter"
    [ tC "filter1" $ filter even [1..10] @?= [2,4,6,8,10]
    , tC "filter2" $ filter odd [1..10] @?=  [1,3,5,7,9]
    , tC "filter3" $ filter (const True) [] @?= ([] :: [Int])
    ]

  , testGroup "foldr"
    [ tC "foldr1" $ foldr (+) 0 [1..10] @?= 55
    ]

----------------------------------------------------------------------
-- BONUS: Add unit or prop tests until you're sure everything works --
----------------------------------------------------------------------
  , testGroup "bonus" []
  ]
  where tC = testCase

main :: IO ()
main = defaultMain test
