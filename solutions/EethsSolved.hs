module Eeths where

import Control.Applicative
import Control.Monad
import Data.Monoid

data E l r = L l | R r deriving (Show, Eq)

instance Functor (E l) where
  fmap _ (L l) = L l
  fmap f (R r) = R (f r)

instance Applicative (E l) where
  pure = R
  (L l) <*> _ = L l
  _ <*> (L l) = L l
  (R f) <*> (R r) = R (f r)

instance Monoid l => Alternative (E l) where
  empty = L mempty
  (R r) <|> _ = R r
  (L _) <|> (R r) = R r
  -- error-handling: keep the first error encountered
  (L l) <|> (L _) = L l

instance Monad (E l) where
  return = pure
  (L l) >>= _ = L l
  (R r) >>= m = m r

instance Monoid l => MonadPlus (E l) where
  mzero = empty
  mplus = (<|>)

instance Monoid r => Monoid (E l r) where
  mempty = mempty
  mappend (L l) _ = L l
  mappend _ (L l) = L l
  mappend (R r1) (R r2) = R (mappend r1 r2)

main :: IO ()
main = print [a, b, c, d, e, f, g, h]
  where a = L "err" <|> R 1
        b = r1 >>= (R . (+1))
        c = (+) <$> r1 <*> r2
        d = (+) <$> l1 <*> r2
        e = (+) <$> r1 <*> l2
        f = (+) <$> l1 <*> l2
        g = r1 <|> r1
        h = l1 <|> l2
        r1 = R 1 :: E String Int
        r2 = R 2 :: E String Int
        l1 = L "err1" :: E String Int
        l2 = L "err2" :: E String Int
